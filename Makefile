.PHONY: help all \
				server-all-in-one \
				check-tool-docker check-tool-ansible \
				fresh-k8s-cluster \
				show-supported-storage-plugins check-storage-plugin storage-plugin-install \
				test-fio combined \
				full full-rook-ceph-lvm full-linstor-drbd9 full-openebs-mayastor-nvmf \
				full-openebs-mayastor-iscsi full-openebs-cstor full-openebs-jiva \
				full-openebs-localpv-hostpath full-openebs-localpv-zfs

# COLORS
RED    := $(shell tput -Txterm setaf 1)
GREEN  := $(shell tput -Txterm setaf 2)
YELLOW := $(shell tput -Txterm setaf 3)
VIOLET := $(shell tput -Txterm setaf 5)
AQUA   := $(shell tput -Txterm setaf 6)
WHITE  := $(shell tput -Txterm setaf 7)
RESET  := $(shell tput -Txterm sgr0)

## Show help
help:
	@echo ''
	@echo 'Makefile for k8s-storage-provider-benchmarks'
	@echo ''
	@echo 'Usage:'
	@echo '  ${YELLOW}make${RESET} ${GREEN}<target>${RESET}'
	@echo ''
	@echo 'Env Variables:'
	@printf "  ${YELLOW}STORAGE_PLUGIN${RESET}\tSet storage plugin, [*rook-ceph-lvm*, linstor-drbd9, ...]\n"
	@printf "  ${YELLOW}TEST${RESET}\tWhich test to run, [*fio*]\n"
	@printf "  ${YELLOW}STORAGE_CLASS${RESET}\tWhich storage class to use, [*jbod*, raid1]\n"
	@printf "  ${YELLOW}FIO_DIRECT_WRITE${RESET}\tWhether to use direct writes with fio, [*off*, on]\n"
	@echo ''
	@echo 'Targets:'
	@awk '/^[a-zA-Z\-_0-9]+:/ { \
		helpMessage = match(lastLine, /^## (.*)/); \
		if (helpMessage) { \
			helpCommand = substr($$1, 0, index($$1, ":")-1); \
			helpMessage = substr(lastLine, RSTART + 2, RLENGTH); \
			printf "  ${YELLOW}%-$(TARGET_MAX_CHAR_NUM)s${RESET}\t${GREEN}%s${RESET}\n", helpCommand, helpMessage; \
		} \
	} \
	{ lastLine = $$0 }' $(MAKEFILE_LIST)

## Build an all-in-one server
server-all-in-one: fresh-k8s-cluster storage-plugin-install

RESULTS_DIR ?= results

###########
# Tooling #
###########

ANSIBLE ?= ansible
KUBIE ?= kubie

check-tool-ansible:
ifeq (, $(shell which $(ANSIBLE)))
	$(error "`ansible` is not available please install ansible (https://ansible.com)")
endif

check-tool-kubie:
ifeq (, $(shell which $(KUBIE)))
	$(error "`kubie` is not available please install kubie (https://github.com/sbstp/kubie)")
endif

## Enter a kubie shell
kubie: check-tool-kubie
	@echo "Entering a kubie context with the spawned cluster..."
	$(MAKE) -C kubernetes kubie

######################
# Setup & Kubernetes #
######################

fresh-k8s-cluster: check-tool-ansible
	$(MAKE) -C ansible

###################################
# Storage plugin k8s installation #
###################################

SUPPORTED_STORAGE_PLUGINS = rook-ceph-lvm \
	linstor-drbd9 \
	openebs-mayastor-nvmf \
	openebs-mayastor-iscsi \
	openebs-cstor \
	openebs-jiva \
	openebs-localpv-hostpath \
	openebs-localpv-zfs

## Which storage plugin to use
STORAGE_PLUGIN ?= rook-ceph-lvm

## Show supported storage plugins (STORAGE_PLUGIN)
show-supported-storage-plugins:
	@echo "Supported storage plugins: [$(SUPPORTED_STORAGE_PLUGINS)]"

check-storage-plugin:
ifeq (,$(filter $(STORAGE_PLUGIN), $(SUPPORTED_STORAGE_PLUGINS)))
	$(error "Unrecognized/unsupported storage plugin [${STORAGE_PLUGIN}]")
endif

storage-plugin-install: check-storage-plugin
ifeq ("rook-ceph-lvm","$(STORAGE_PLUGIN)")
	$(MAKE) -C kubernetes/rook
else ifeq ("openebs-mayastor-nvmf","$(STORAGE_PLUGIN)")
	$(MAKE) -C kubernetes/openebs/mayastor
else ifeq ("openebs-mayastor-isci","$(STORAGE_PLUGIN)")
	$(MAKE) -C kubernetes/openebs/mayastor
else ifeq ("openebs-cstor","$(STORAGE_PLUGIN)")
	$(MAKE) -C kubernetes/openebs/common
	$(MAKE) -C kubernetes/openebs/cstor
else ifeq ("openebs-jiva","$(STORAGE_PLUGIN)")
	$(MAKE) -C kubernetes/openebs/common
	$(MAKE) -C kubernetes/openebs/jiva
else ifeq ("openebs-localpv-hostpath","$(STORAGE_PLUGIN)")
	$(MAKE) -C kubernetes/openebs/common
	$(MAKE) -C kubernetes/openebs/localpv-hostpath
else ifeq ("openebs-localpv-zfs","$(STORAGE_PLUGIN)")
	$(MAKE) -C kubernetes/openebs/localpv-zfs
else ifeq ("linstor-drbd9","$(STORAGE_PLUGIN)")
	$(MAKE) -C kubernetes/linstor
else
	$(error "Unrecognized/unsupported storage plugin [${STORAGE_PLUGIN}]")
endif

###########
# Testing #
###########

## Available tests
SUPPORTED_TESTS = fio pgbench

## Default test
TEST ?= fio

## Show supported storage plugins (STORAGE_PLUGIN)
show-supported-tests:
	@echo "Supported storage classes: [$(SUPPORTED_TESTS)]"

check-test:
ifeq (,$(filter $(TEST), $(SUPPORTED_TESTS)))
	$(error "Unrecognized/unsupported test [${TEST}]")
endif

## What storage class to use
SUPPORTED_STORAGE_CLASSES = jbod raid1

## Which storage class
STORAGE_CLASS ?= jbod

## Show supported storage plugins (STORAGE_PLUGIN)
show-supported-storage-classes:
	@echo "Supported storage classes: [$(SUPPORTED_STORAGE_CLASSES)]"

check-storage-class:
ifeq (,$(filter $(STORAGE_CLASS), $(SUPPORTED_STORAGE_CLASSES)))
	$(error "Unrecognized/unsupported storage class [${STORAGE_CLASS}]")
endif

## Whether to use direct write or not (usually on or off
FIO_DIRECT_WRITE ?= off

test: check-test check-storage-class
ifeq ("fio","$(TEST)")
	export STORAGE_PLUGIN
	export STORAGE_CLASS
	export FIO_DIRECT_WRITE
	export TEST
	$(MAKE) -C kubernetes/tests/fio
else ifeq ("pgbench","$(TEST)")
	export STORAGE_PLUGIN
	export STORAGE_CLASS
	export TEST
	$(MAKE) -C kubernetes/tests/pgbench
else
	$(error "Unrecognized/unsupported top-level test [${TEST}]")
endif

########################
# Combined Test Suite  #
########################

full: full-rook-ceph-lvm full-linstor-drbd9 full-openebs-mayastor \
			full-openebs-cstor full-openebs-jiva full-openebs-localpv-hostpath \
			full-openebs-localpv-zfs

full-rook-ceph-lvm:
	@echo "\n[info] [$(STORAGE_PLUGIN)] setting up machine..."
	STORAGE_PLUGIN=rook-ceph-lvm $(MAKE) fresh-k8s-cluster storage-plugin-install

	@echo "\n[info] [$(STORAGE_PLUGIN)] running fio tests..."
	STORAGE_PLUGIN=rook-ceph-lvm STORAGE_CLASS=jbod TEST=fio FIO_DIRECT_WRITE=off $(MAKE) test
	STORAGE_PLUGIN=rook-ceph-lvm STORAGE_CLASS=jbod TEST=fio FIO_DIRECT_WRITE=on $(MAKE) test
	STORAGE_PLUGIN=rook-ceph-lvm STORAGE_CLASS=raid1 TEST=fio FIO_DIRECT_WRITE=off $(MAKE) test
	STORAGE_PLUGIN=rook-ceph-lvm STORAGE_CLASS=raid1 TEST=fio FIO_DIRECT_WRITE=on $(MAKE) test

	@echo "\n[info] [$(STORAGE_PLUGIN)] running pgbench tests..."
	STORAGE_PLUGIN=rook-ceph-lvm STORAGE_CLASS=jbod TEST=pgbench $(MAKE) test
	STORAGE_PLUGIN=rook-ceph-lvm STORAGE_CLASS=raid1 TEST=pgbench $(MAKE) test

full-linstor:
	@echo "\n[info] [$(STORAGE_PLUGIN)] setting up machine..."
	STORAGE_PLUGIN=linstor-drbd9 $(MAKE) fresh-k8s-cluster storage-plugin-install

	@echo "[info] [$(STORAGE_PLUGIN)] running fio tests..."
	STORAGE_PLUGIN=linstor-drbd9 STORAGE_CLASS=jbod TEST=fio FIO_DIRECT_WRITE=off $(MAKE)  test

	@echo "[info] [$(STORAGE_PLUGIN)] running pgbench tests..."
	STORAGE_PLUGIN=linstor-drbd9 STORAGE_CLASS=jbod TEST=pgbench $(MAKE) test

# NOTE: there is no single-node RAID1 for Mayastor
# see NOTE in kubernetes/openebs/mayastor/Makefile
# TODO: re-add RAID1 runs for clustered mode

full-openebs-mayastor:
	@echo "\n[info] [$(STORAGE_PLUGIN)] setting up machine..."
	STORAGE_PLUGIN=openebs-mayastor-nvmf $(MAKE) fresh-k8s-cluster storage-plugin-install

	@echo "\n[info] [$(STORAGE_PLUGIN)] running fio tests..."
	STORAGE_PLUGIN=openebs-mayastor-nvmf STORAGE_CLASS=jbod TEST=fio FIO_DIRECT_WRITE=off $(MAKE) test
	STORAGE_PLUGIN=openebs-mayastor-nvmf STORAGE_CLASS=jbod TEST=fio FIO_DIRECT_WRITE=on $(MAKE) test

	@echo "\n[info] [$(STORAGE_PLUGIN)] running pgbench tests..."
	STORAGE_PLUGIN=openebs-mayastor-nvmf STORAGE_CLASS=jbod TEST=pgbench $(MAKE) test

	@echo "\n[info] [$(STORAGE_PLUGIN)] running fio tests..."
	STORAGE_PLUGIN=openebs-mayastor-iscsi STORAGE_CLASS=jbod TEST=fio FIO_DIRECT_WRITE=off $(MAKE) test
	STORAGE_PLUGIN=openebs-mayastor-iscsi STORAGE_CLASS=jbod TEST=fio FIO_DIRECT_WRITE=on $(MAKE) test

	@echo "\n[info] [$(STORAGE_PLUGIN)] running pgbench tests..."
	STORAGE_PLUGIN=openebs-mayastor-iscsi STORAGE_CLASS=jbod TEST=pgbench $(MAKE) test

# NOTE: there is no JBOD (for our current definition of jbod/raid1 being @ the disk level) for cStor
# see NOTE in kubernetes/openebs/cstor/Makefile
# TODO: once clustered setup is available add a raid1-ha

full-openebs-cstor:
	@echo "\n[info] [$(STORAGE_PLUGIN)] setting up machine..."
	STORAGE_PLUGIN=openebs-cstor $(MAKE) fresh-k8s-cluster storage-plugin-install

	@echo "\n[info] [$(STORAGE_PLUGIN)] running fio tests..."
	STORAGE_PLUGIN=openebs-cstor STORAGE_CLASS=raid1 TEST=fio FIO_DIRECT_WRITE=off $(MAKE) test
	STORAGE_PLUGIN=openebs-cstor STORAGE_CLASS=raid1 TEST=fio FIO_DIRECT_WRITE=on $(MAKE) test

	@echo "\n[info] [$(STORAGE_PLUGIN)] running pgbench tests..."
	STORAGE_PLUGIN=openebs-cstor STORAGE_CLASS=raid1 TEST=pgbench $(MAKE) test

full-openebs-jiva:
	@echo "\n[info] [$(STORAGE_PLUGIN)] setting up machine..."
	STORAGE_PLUGIN=openebs-jiva $(MAKE) fresh-k8s-cluster storage-plugin-install

	@echo "\n[info] [$(STORAGE_PLUGIN)] running fio tests..."
	STORAGE_PLUGIN=openebs-jiva STORAGE_CLASS=jbod TEST=fio FIO_DIRECT_WRITE=off $(MAKE) test
	STORAGE_PLUGIN=openebs-jiva STORAGE_CLASS=jbod TEST=fio FIO_DIRECT_WRITE=on $(MAKE) test

	@echo "\n[info] [$(STORAGE_PLUGIN)] running pgbench tests..."
	STORAGE_PLUGIN=openebs-jiva STORAGE_CLASS=jbod TEST=pgbench $(MAKE) test

full-openebs-localpv-hostpath:
	@echo "\n[info] [$(STORAGE_PLUGIN)] setting up machine..."
	STORAGE_PLUGIN=openebs-localpv-hostpath $(MAKE) fresh-k8s-cluster storage-plugin-install

	@echo "\n[info] [$(STORAGE_PLUGIN)] running fio tests..."
	STORAGE_PLUGIN=openebs-localpv-hostpath STORAGE_CLASS=jbod TEST=fio FIO_DIRECT_WRITE=off $(MAKE) test
	STORAGE_PLUGIN=openebs-localpv-hostpath STORAGE_CLASS=jbod TEST=fio FIO_DIRECT_WRITE=on $(MAKE) test

	@echo "\n[info] [$(STORAGE_PLUGIN)] running pgbench tests..."
	STORAGE_PLUGIN=openebs-localpv-hostpath STORAGE_CLASS=jbod TEST=pgbench $(MAKE) test

full-openebs-localpv-zfs:
	@echo "\n[info] [$(STORAGE_PLUGIN)] setting up machine..."
	STORAGE_PLUGIN=openebs-localpv-zfs $(MAKE) fresh-k8s-cluster storage-plugin-install

	@echo "\n[info] [$(STORAGE_PLUGIN)] running fio tests..."
	STORAGE_PLUGIN=openebs-localpv-zfs STORAGE_CLASS=raid1 TEST=fio FIO_DIRECT_WRITE=off $(MAKE) test
	STORAGE_PLUGIN=openebs-localpv-zfs STORAGE_CLASS=raid1 TEST=fio FIO_DIRECT_WRITE=on $(MAKE) test

	@echo "\n[info] [$(STORAGE_PLUGIN)] running pgbench tests..."
	STORAGE_PLUGIN=openebs-localpv-zfs STORAGE_CLASS=raid1 TEST=pgbench $(MAKE) test
