#
# Playbook for setting up kubernetes
#
---
- name: k8s-setup all-in-one server setup
  hosts: "{{ ansible_limit | default(omit) }}"
  remote_user: root
  vars:
    storage_plugin: "{{ lookup('env', 'STORAGE_PLUGIN') }}"
    k8s_node_name: "{{ inventory_hostname.split('.') | first }}"

    k0s_version: v0.12.0
    k0s_download_url: "https://github.com/k0sproject/k0s/releases/download/{{ k0s_version }}/k0s-{{ k0s_version }}-amd64"
    k0s_checksum: sha256:0a3ead8f8e5f950390eeb76bd39611d1754b282536e8d9dbbaa0676550c2edbf

    kubectl_version: v1.20.0 # cluster version
    kubectl_download_url: "https://dl.k8s.io/release/{{ kubectl_version }}/bin/linux/amd64/kubectl"
    kubectl_checksum: sha256:a5895007f331f08d2e082eb12458764949559f30bcc5beae26c38f3e2724262c

  tasks:
    - name: Populate service facts
      ansible.builtin.service_facts:

    - name: Download k0s
      ansible.builtin.get_url:
        url: "{{ k0s_download_url }}"
        checksum: "{{ k0s_checksum }}"
        mode: 0755
        dest: /usr/bin/k0s
      when: ansible_facts.services["k0scontroller.service"] is not defined

    - name: Create /var/lib/k0s folder
      ansible.builtin.file:
        path: /var/lib/k0s
        state: directory
      when: ansible_facts.services["k0scontroller.service"] is not defined

    - name: Add k0s config file
      ansible.builtin.template:
        src: k0s-config.yaml.j2
        dest: /var/lib/k0s/config.yaml
        owner: root
        group: root
        mode: 0644
      when: ansible_facts.services["k0scontroller.service"] is not defined

    - name: Install k0s
      ansible.builtin.command: |
        k0s install controller -c /var/lib/k0s/config.yaml --single
      when: ansible_facts.services["k0scontroller.service"] is not defined

    - name: Start the k0s service
      ansible.builtin.systemd:
        name: k0scontroller
        state: started
        enabled: yes
      when: ansible_facts.services["k0scontroller.service"] is not defined or ansible_facts.services["k0scontroller.service"].state != "running"

    - name: Create worker join token (saved @ /tmp/worker-token)
      shell: |
        k0s token create --role=worker --expiry=168h > /tmp/worker-token

    - name: Copy out worker token
      ansible.builtin.fetch:
        src: /tmp/worker-token
        dest: output

    - name: Copy out cluster configuration
      ansible.builtin.fetch:
        src: /var/lib/k0s/pki/admin.conf
        dest: output

    - name: Replace localhost in cluster configuration
      delegate_to: localhost
      ansible.builtin.replace:
        path: "output/{{ inventory_hostname }}/var/lib/k0s/pki/admin.conf"
        regexp: 'https://localhost:6443'
        replace: "https://{{ cluster_external_address | default(inventory_hostname) }}:6443"

    - name: (ufw) Allow TCP access on port 6443
      tags: [ "ufw" ]
      community.general.ufw:
        rule: allow
        port: '6443'
        proto: tcp

    - name: (ufw) Allow UDP access on port 6443
      tags: [ "ufw" ]
      community.general.ufw:
        rule: allow
        port: '6443'
        proto: udp

    - name: (ufw) Allow out access on vxlan.calico
      tags: [ "ufw" ]
      community.general.ufw:
        rule: allow
        direction: in
        interface: vxlan.calico

    - name: (ufw) Allow in access on vxlan.calico
      tags: [ "ufw" ]
      community.general.ufw:
        rule: allow
        direction: in
        interface: vxlan.calico

    - name: Download and install kubectl
      ansible.builtin.get_url:
        url: "{{ kubectl_download_url }}"
        checksum: "{{ kubectl_checksum }}"
        mode: 0755
        dest: /usr/local/bin/kubectl

    - name: Wait (~3 minutes) for kubernetes node to finish setting up
      command: |
        kubectl --kubeconfig=/var/lib/k0s/pki/admin.conf get node {{ k8s_node_name }}
      retries: 6
      delay: 30
      register: node_listing
      until: node_listing.rc == 0

    - name: Label the node mayastor-capable
      when: storage_plugin in target_plugins
      ansible.builtin.command: |
        kubectl --kubeconfig=/var/lib/k0s/pki/admin.conf label node --overwrite {{ k8s_node_name }} openebs.io/engine=mayastor
      vars:
        target_plugins:
          - openebs-mayastor-nvmf
          - openebs-mayastor-iscsi

    - name: Label the node zfs-capable
      when: storage_plugin in target_plugins
      ansible.builtin.command: |
        kubectl --kubeconfig=/var/lib/k0s/pki/admin.conf label node --overwrite {{ k8s_node_name }} zfs-support=yes
      vars:
        target_plugins:
          - openebs-localpv-zfs

    - name: Label the node linstor-capable
      when: storage_plugin in target_plugins
      ansible.builtin.command: |
        kubectl --kubeconfig=/var/lib/k0s/pki/admin.conf label node --overwrite {{ k8s_node_name }} linstor-support=yes
      vars:
        target_plugins:
          - linstor-drbd9

    # If settings don't survive a reboot, they're not real
    - name: Reboot the machine to finish installation
      ansible.builtin.reboot:
