# Ansible #

## Getting Started ##

### Set up DNS and update the Inventory file ###

**NOTE** Scripts assume that the first entry in your server's DNS name is the machine name (ex. `server-01.domain.tld` will have `hostname` set to `server-01`)

Make your machine easier to reference by setting up DNS A and/or AAAA record(s) to point at the server. So if the raw IP is `1.2.3.4`, making an A record named `myserver.domain.tld` to point at it makes it much easier to perform commands against that server.

After you've done that, update the inventory file @ `inventory.yaml`, you'll likely want to replace `all-in-one-01.k8s.storage-benchmarks.experiments.vadosware.io` (which is what I used) with `<your chosen DNS name>`.

## Playbooks ##

The playbooks are meant to be run in this order:

- `server-reset.yml`
- `pre-ansible-setup.yml`
- `post-ansible-setup.yml`
- `storage-plugin-setup.yml`
- `k8s-install.yml`

All playbooks require the `--limit` switch to be set so users are forced to think about on which machines things will run:

```console
$ ansible playbook.yml --limit <host>
```

### `server-reset.yml`

Completely wipe & reset a server

### `rescue-mode-setup.yml`

Enable and enter the [Hetzner Rescue System](https://docs.hetzner.com/robot/dedicated-server/troubleshooting/hetzner-rescue-system/)

Disable software RAID (if present)

### `pre-ansible-setup.yml`

Set up requirements for Ansible itself

### `storage-plugin-setup.yml`

Set up pre-requisites for a given storage plugin; installing kernel modules, packages, etc.

### `k8s-install.yml`

Set up pre-requisites for installing k8s; which may mean installing binaries, setting up folders, etc.

Install Kubernetes on the given node.

## Examples ##

### Re-run from a certain task ###

```console
$ make server-reset ANSIBLE_ARGS="--start-at-task='Generate local SSH key'"
```

**NOTE** if you are trying to run a task with a [`block`](https://docs.ansible.com/ansible/latest/user_guide/playbooks_blocks.html), you will not be able to target the outer task by it's name -- you can add a tag and use that instead.

### Run only tasks with a certain tag ###

```console
$ make server-reset ANSIBLE_ARGS="--tags='drive-partition-prep"
```

**NOTE**: Generally, you'll want to create a new tag to target specific roles rather than trying to combine tags -- the tags combine with an OR function, *not* AND. So `tags=a,b` will run tasks tagged with `"a"` and `"b"`.

### Get in context of a successfully built cluster ###

```console
kubie ctx -f output/
```

## Issues ##

### I'm stuck in rescue mode! ###

No you're not (probably) -- reset the machine one more time through the Hetzner console and the machine should boot normally. From there, you have another chance to run the scripts and/or enter rescue mdoe again some other way.
