.PHONY: all run \
        pvc-yaml pvc \
        job job-wait \
        collect-results

KUBIE ?= kubie
KUBECTL_BIN ?= kubectl
KUBECTL ?= $(KUBECTL_BIN) --kubeconfig=$(KUBECONFIG_PATH)
KUSTOMIZE ?= $(KUBECTL) kustomize

EXPECTED_ADMIN_CONFIG_PATH ?= $(shell realpath ../../../ansible/output/**/var/lib/k0s/pki/admin.conf)
KUBECONFIG_PATH ?= $(EXPECTED_ADMIN_CONFIG_PATH)

RESULTS_DIR_PATH ?= $(shell realpath ../../../results)
JOB_LOG_FILE_NAME ?= $(STORAGE_PLUGIN)-$(TEST)-$(STORAGE_CLASS)-direct-write-$(FIO_DIRECT_WRITE).log
JOB_LOG_FILE_PATH ?= $(RESULTS_DIR_PATH)/$(JOB_LOG_FILE_NAME)

OVERLAY_PATH ?= overlays/$(STORAGE_PLUGIN)/$(STORAGE_CLASS)/direct-write-$(FIO_DIRECT_WRITE)

STORAGE_PLUGIN ?= rook-ceph-lvm
STORAGE_CLASS ?= jbod
TEST ?= fio

FIO_DIRECT_WRITE ?= off

ifeq ("on","$(FIO_DIRECT_WRITE)")
	FIO_DIRECT = 1
else
	FIO_DIRECT = 0
endif

all: clean job job-wait collect-results job-uninstall

clean:
	@echo -e "\n[info] *** clearing out job that may or may not exist ***"
	$(KUBECTL) delete -k $(OVERLAY_PATH) || true

job:
	$(KUBECTL) apply -k $(OVERLAY_PATH)

job-uninstall:
	$(KUBECTL) delete -k $(OVERLAY_PATH)

job-wait:
	@echo "Waiting for job to finish (timeout 30m)..."
	$(KUBECTL) wait job/fio --for=condition=complete --timeout=30m

collect-results:
	@echo "Writing job/fio log output to [./results/$(JOB_LOG_FILE_PATH)]..."
	$(KUBECTL) logs job/fio > $(JOB_LOG_FILE_PATH)
