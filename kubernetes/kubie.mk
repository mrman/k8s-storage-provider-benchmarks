.PHONY: kubie

KUBIE ?= kubie

kubie:
ifeq (,$(EXPECTED_ADMIN_CONFIG_PATH))
	$(error "EXEPECTED_ADMIN_CONFIG_PATH not set!")
else
	$(KUBIE) ctx -f $(EXPECTED_ADMIN_CONFIG_PATH)
endif
