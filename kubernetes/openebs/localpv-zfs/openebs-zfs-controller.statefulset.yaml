---
kind: StatefulSet
apiVersion: apps/v1
metadata:
  name: openebs-zfs-controller
  namespace: openebs
  labels:
    openebs.io/component-name: openebs-zfs-controller
    openebs.io/version: 1.5.0
spec:
  selector:
    matchLabels:
      app: openebs-zfs-controller
      role: openebs-zfs
  serviceName: "openebs-zfs"
  replicas: 1
  template:
    metadata:
      labels:
        app: openebs-zfs-controller
        role: openebs-zfs
        openebs.io/component-name: openebs-zfs-controller
        openebs.io/version: 1.5.0
    spec:
      affinity:
        podAntiAffinity:
          requiredDuringSchedulingIgnoredDuringExecution:
          - labelSelector:
              matchExpressions:
              - key: app
                operator: In
                values:
                - openebs-zfs-controller
            topologyKey: "kubernetes.io/hostname"
      priorityClassName: system-cluster-critical
      serviceAccount: openebs-zfs-controller-sa
      containers:
        - name: csi-resizer
          image: k8s.gcr.io/sig-storage/csi-resizer:v1.1.0
          args:
            - "--v=5"
            - "--csi-address=$(ADDRESS)"
            - "--leader-election"
          env:
            - name: ADDRESS
              value: /var/lib/csi/sockets/pluginproxy/csi.sock
          imagePullPolicy: IfNotPresent
          volumeMounts:
            - name: socket-dir
              mountPath: /var/lib/csi/sockets/pluginproxy/
        - name: csi-snapshotter
          image: k8s.gcr.io/sig-storage/csi-snapshotter:v4.0.0
          imagePullPolicy: IfNotPresent
          args:
            - "--csi-address=$(ADDRESS)"
            - "--leader-election"
          env:
            - name: ADDRESS
              value: /var/lib/csi/sockets/pluginproxy/csi.sock
          volumeMounts:
            - name: socket-dir
              mountPath: /var/lib/csi/sockets/pluginproxy/
        - name: snapshot-controller
          image: k8s.gcr.io/sig-storage/snapshot-controller:v4.0.0
          args:
            - "--v=5"
            - "--leader-election=true"
          imagePullPolicy: IfNotPresent
        - name: csi-provisioner
          image: k8s.gcr.io/sig-storage/csi-provisioner:v2.1.0
          imagePullPolicy: IfNotPresent
          args:
            - "--csi-address=$(ADDRESS)"
            - "--v=5"
            - "--feature-gates=Topology=true"
            - "--strict-topology"
            - "--leader-election"
            - "--extra-create-metadata=true"
          env:
            - name: ADDRESS
              value: /var/lib/csi/sockets/pluginproxy/csi.sock
          volumeMounts:
            - name: socket-dir
              mountPath: /var/lib/csi/sockets/pluginproxy/
        - name: openebs-zfs-plugin
          image: openebs/zfs-driver:1.5.0
          imagePullPolicy: IfNotPresent
          env:
            - name: OPENEBS_CONTROLLER_DRIVER
              value: controller
            - name: OPENEBS_CSI_ENDPOINT
              value: unix:///var/lib/csi/sockets/pluginproxy/csi.sock
            - name: OPENEBS_NAMESPACE
              value: openebs
            - name: OPENEBS_IO_INSTALLER_TYPE
              value: "zfs-operator"
            - name: OPENEBS_IO_ENABLE_ANALYTICS
              value: "false"
          args :
            - "--endpoint=$(OPENEBS_CSI_ENDPOINT)"
            - "--plugin=$(OPENEBS_CONTROLLER_DRIVER)"
          volumeMounts:
            - name: socket-dir
              mountPath: /var/lib/csi/sockets/pluginproxy/
      volumes:
        - name: socket-dir
          emptyDir: {}
