.PHONY: install uninstall get-kubeconfig kubie \
				blockdevice blockdevice-uninstall blockdevice-crd-wait \
				storagepoolclaim storagepoolclaim-uninstall storagepoolclaim-crd-wait \
				storageclass storageclass-uninstall storageclass-crd-wait \
				test test-uninstall \
				test-raid1 test-raid1-uninstall

EXPECTED_ADMIN_CONFIG_PATH ?= $(shell realpath ../../../ansible/output/**/var/lib/k0s/pki/admin.conf)
KUBECONFIG_PATH ?= $(EXPECTED_ADMIN_CONFIG_PATH)

KUBECTL_BIN ?= kubectl
KUBIE ?= kubie
KUBECTL ?= $(KUBECTL_BIN) --kubeconfig=$(KUBECONFIG_PATH)

install: blockdevice storagepoolclaim storageclass

uninstall: storageclass-uninstall storagepoolclaim-uninstall blockdevice-uninstall

include ./../../kubie.mk

get-kubeconfig:
	@echo $(KUBECONFIG_PATH)

################
# Blockdevices #
################

blockdevice-crd-wait:
	@echo "[info] waiting until blockdevice CRD is installed..."
	@until $(KUBECTL) -n openebs get blockdevice; \
			do echo "trying again in 20 seconds (ctrl+c to cancel)"; \
			sleep 20; \
	done

# NOTE: we can't use the second disk in it's entirety because ZFS needs identical
# disks for a mirror, and the leftover partition on the first disk is small
blockdevice: blockdevice-crd-wait
	$(KUBECTL) apply -f first-disk-partition.blockdevice.yaml
	$(KUBECTL) apply -f second-disk-partition.blockdevice.yaml

blockdevice-uninstall: blockdevice-crd-wait
	$(KUBECTL) delete -f first-disk-partition.blockdevice.yaml
	$(KUBECTL) delete -f second-disk.blockdevice.yaml

#####################
# StoragePoolClaims #
#####################

storagepoolclaim-crd-wait:
	@echo "[info] waiting until storagepoolclaim CRD is installed..."
	@until $(KUBECTL) -n openebs get storagepoolclaim; \
			do echo "trying again in 20 seconds (ctrl+c to cancel)"; \
			sleep 20; \
	done

# NOTE: There is no JBOD, disks must be striped/mirrored
# TODO: Clarify between raid1 on one node and raid1 on 2+nodes (raid1 vs raid1-ha?, and annotations/labels?)

storagepoolclaim: storagepoolclaim-crd-wait
	$(KUBECTL) apply -f raid1.storagepoolclaim.yaml

storagepoolclaim-uninstall: storagepoolclaim-crd-wait
	$(KUBECTL) delete -f raid1.storagepoolclaim.yaml

##################
# Storageclasses #
##################

CSTOR_POOL_SELECTOR ?= openebs.io/storage-pool-claim=raid1

disk-pool-available:
	@echo "[info] waiting until selector [$(CSTOR_POOL_SELECTOR)] returns an single 'healthy' entry..."
	@while : ; do \
			export _POOL_WC=`$(KUBECTL) -n openebs get csp --selector=$(CSTOR_POOL_SELECTOR) | grep 'Healthy' | wc -l`; \
			[[ "$$_POOL_WC" == "1" ]] && break; \
			echo "failed to find healthy cstor pool with selector, trying again in 20 seconds (ctrl+c to cancel)"; \
			sleep 20; \
	done
	@echo "[info] detected cstor pool with selector [$(CSTOR_POOL_SELECTOR)] ..."

storageclass: disk-pool-available
	$(KUBECTL) apply -f raid1.storageclass.yaml

storageclass-uninstall: disk-pool-available
	$(KUBECTL) delete -f raid1.storageclass.yaml

##################
# Test resources #
##################

test: test-raid1

test-uninstall: test-raid1-uninstall

test-raid1:
	$(KUBECTL) apply -f test-raid1.pvc.yaml
	$(KUBECTL) apply -f test-raid1.pod.yaml

test-raid1-uninstall:
	$(KUBECTL) delete -f test-raid1.pod.yaml
	$(KUBECTL) delete -f test-raid1.pvc.yaml
