.PHONY: install uninstall get-kubeconfig kubie \
				storagepool storagepool-uninstall \
				storageclass storageclass-uninstall \
				test test-uninstall \
				test-jbod test-jbod-uninstall

EXPECTED_ADMIN_CONFIG_PATH ?= $(shell realpath ../../../ansible/output/**/var/lib/k0s/pki/admin.conf)
KUBECONFIG_PATH ?= $(EXPECTED_ADMIN_CONFIG_PATH)

KUBECTL_BIN ?= kubectl
KUBIE ?= kubie
KUBECTL ?= $(KUBECTL_BIN) --kubeconfig=$(KUBECONFIG_PATH)

install: storagepool storageclass

uninstall: storageclass-uninstall storagepool-uninstall

include ./../../kubie.mk

get-kubeconfig:
	@echo $(KUBECONFIG_PATH)

################
# StoragePools #
################

storagepool-crd-wait:
	@echo "[info] waiting until storagepool CRD is installed..."
	@until $(KUBECTL) -n openebs get storagepool; \
			do echo "trying again in 20 seconds (ctrl+c to cancel)"; \
			sleep 20; \
	done

storagepool: storagepool-crd-wait
	$(KUBECTL) apply -f first-disk-partition.storagepool.yaml
	$(KUBECTL) apply -f second-disk.storagepool.yaml

storagepool-uninstall: storagepool-crd-wait
	$(KUBECTL) delete -f first-disk-partition.storagepool.yaml
	$(KUBECTL) delete -f second-disk.storagepool.yaml

##################
# Storageclasses #
##################

storageclass:
	$(KUBECTL) apply -f openebs-jiva-d2-replicated.storageclass.yaml
	$(KUBECTL) apply -f openebs-jiva-d2-single.storageclass.yaml
	$(KUBECTL) apply -f openebs-jiva-d1-replicated.storageclass.yaml
	$(KUBECTL) apply -f openebs-jiva-d1-single.storageclass.yaml
	$(KUBECTL) apply -f jbod.storageclass.yaml

storageclass-uninstall: disk-pool-available
	$(KUBECTL) delete -f openebs-jiva-d2-replicated.storageclass.yaml
	$(KUBECTL) delete -f openebs-jiva-d2-single.storageclass.yaml
	$(KUBECTL) delete -f openebs-jiva-d1-replicated.storageclass.yaml
	$(KUBECTL) delete -f openebs-jiva-d1-single.storageclass.yaml
	$(KUBECTL) delete -f jbod.storageclass.yaml

##################
# Test resources #
##################

## NOTE: There is no RAID1 because Jiva takes folders on disks and the disks are provisioned as separate underneath.
## AFAIK Jiva there's no real way to get Jiva to make multiple replicas on the same node (thought I thought it was possible),
## this means that single node "RAID1" JIVA setups need to have software RAID happening at the layer *below* Jiva

test: test-jbod

test-uninstall: test-jbod-uninstall test-raid1-uninstall

test-jbod:
	$(KUBECTL) apply -f test-jbod.pvc.yaml
	$(KUBECTL) apply -f test-jbod.pod.yaml

test-jbod-uninstall:
	$(KUBECTL) delete -f test-jbod.pod.yaml
	$(KUBECTL) delete -f test-jbod.pvc.yaml
