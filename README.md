# Kubernetes storage provider benchmarks #

Repository with drive benchmarking code

## Setup ##

### Set up `direnv` ###

Set up [`direnv`](https://direnv.net/) by creating a `.envrc` file in this folder, an example:

```bash
export TARGET=all-in-one-01.k8s.storage-benchmarks.experiments.vadosware.io
```

### Get a Hetzner Robot WebService Account ###

See the [WebService API documentation](https://robot.your-server.de/doc/webservice/en.html), it will direct you to do the following:

> In order to use the interface you need to create a webservice user in Robot under "Settings; Webservice and app settings".

That information should be added to your environment by way of modifying the `.envrc` you have (or manually `export`ing):

```bash
export TARGET=all-in-one-01.k8s.storage-benchmarks.experiments.vadosware.io

export HETZNER_WEBSERVICE_USERNAME=<your username>
export HETZNER_WEBSERVICE_PASSWORD=<your password>
```

These credentials are required for resetting the machines before/in-between runs

### (optional) Install `kubie` ###

This project uses [`kubie`](https://github.com/sbstp/kubie) to open contexts to the created cluster.

### (optional) Customize applicable hard-coded bits ###

To minimize development time, there are a few places where I've chosen to hard-code values, shunning templating tools (ex. `kustomize`/`jsonnet`/`helm`(3)/`envsubst`/etc). If you want to use this repository across your own infrastructure, you will need to change some values in your setup by hand as a result:

- `kubernetes/openebs/mayastor/whole-disk.mayastorpool.yaml`: `node` and `disks` listing
- `ansible/server-reset.yml`: `/dev/nvme[0|1]n1` usage (your first/second drive may look different, for example `/dev/sd[a|b]`)
- `ansible/storage-plugin-setup.yml`: `/dev/nvme[0|1]n1` usage

This information is generally known/confirmed on the ansible side so you'll have to make the

## Quickstart: Set a machine up with `rook-ceph-lvm` ##

To go from existing machine to reset machine + k8s installed and ready for use with `rook-ceph-lvm`:

```console
$ STORAGE_PLUGIN=rook-ceph-lvm make fresh-k8s-cluster storage-plugin-install
```

To see the available `STORAGE_PLUGIN`s, run `make show-supported-storage-plugins`:

```console
$ make show-supported-storage-plugins
Supported storage plugins: [rook-ceph-lvm linstor-drbd9 openebs-mayastor-nvmf openebs-mayastor-iscsi openebs-cstor openebs-jiva openebs-localpv-hostpath openebs-localpv-zfs]
```

## Help ##

You can get help by running the `help` make target:

```console
$ make help

Makefile for k8s-storage-provider-benchmarks

Usage:
  make <target>

Env Variables:
  STORAGE_PLUGIN        Set storage plugin, [*rook-ceph-lvm*, linstor-drbd9, ...]
  TEST  Which test to run, [*fio*]
  STORAGE_CLASS  Which storage class to use, [*jbod*, raid1]
  FIO_DIRECT_WRITE      Whether to use direct writes with fio, [*off*, on]

Targets:
  help   Show help
  server-all-in-one      Build an all-in-one server
  kubie  Enter a kubie shell
  show-supported-storage-plugins         Show supported storage plugins (STORAGE_PLUGIN)
  show-supported-tests   Show supported storage plugins (STORAGE_PLUGIN)
  show-supported-storage-classes  Show supported storage plugins (STORAGE_PLUGIN)
```


## Utilities ##

### Start a `kubie` context to the cluster ###

From the root folder or `kubernetes`, run:

```console
$ make kubie
```

## Ansible ##

Ansible code, instructions, and Makefile targets are in the `ansible` folder

## Kubernetes ##

Kubernetes code, instructions, and Makefile targets are in the `kubernetes` folder
